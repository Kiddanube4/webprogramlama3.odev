﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogSistemi
{
    public struct TBLLOGS
    {
        public Int64 ID { get; internal set; }
        public int KULLANICI_ID { get; set; }

        public string MESAJ { get; set; }
        public string KAYNAGI { get; set; }
      
        public string IP { get; internal set; }
        
        public string PAGE { get; set; }
        
        public string STACKTRACE { get; set; }
     
        public bool IsWeb { get; set; }
      
        public DateTime OLUSMA_ZAMANI { get; set; }
        
    }
}
